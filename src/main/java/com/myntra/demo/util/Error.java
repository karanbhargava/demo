package com.myntra.demo.util;

import lombok.Data;

@Data
public class Error {

    private String code;
    private String message;

    public Error(String code, String message) {
        this.code = code;
        this.message = message;
    }

}
