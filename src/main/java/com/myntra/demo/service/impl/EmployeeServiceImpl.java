package com.myntra.demo.service.impl;

import com.myntra.demo.dao.EmployeeMongoRepository;
import com.myntra.demo.dao.EmployeeRepository;
import com.myntra.demo.exception.NotFoundException;
import com.myntra.demo.model.Employee;
import com.myntra.demo.model.EmployeeMongo;
import com.myntra.demo.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    @Override
    public Employee findById(long id) {
        Employee employee = employeeRepository.findById(id);
        if(employee == null) {
            throw new NotFoundException("No Employee Found in MySql");
        }
        return employee;
    }

    @Override
    public EmployeeMongo findByMongoId(String id) {
        Optional<EmployeeMongo> employeeOptional = employeeMongoRepository.findById(id);
        if(!employeeOptional.isPresent()) {
            throw new NotFoundException("No Mongo Employee Found in Mongo");
        }
        return employeeOptional.get();
    }

    @Override
    public EmployeeMongo saveMongo(EmployeeMongo employeeMongo) {
        return employeeMongoRepository.save(employeeMongo);
    }

    @Autowired
    private EmployeeMongoRepository employeeMongoRepository;

    @Autowired
    private EmployeeRepository employeeRepository;

}
