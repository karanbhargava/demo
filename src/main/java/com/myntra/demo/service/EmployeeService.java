package com.myntra.demo.service;

import com.myntra.demo.model.Employee;
import com.myntra.demo.model.EmployeeMongo;

public interface EmployeeService {

    Employee findById(long id);

    EmployeeMongo findByMongoId(String id);

    EmployeeMongo saveMongo(EmployeeMongo employeeMongo);

}
