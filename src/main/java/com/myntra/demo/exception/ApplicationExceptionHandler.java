package com.myntra.demo.exception;

import com.mongodb.MongoWriteException;
import com.myntra.demo.util.Error;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
@Order(Ordered.HIGHEST_PRECEDENCE)
public class ApplicationExceptionHandler {

	private static Logger logger = LoggerFactory.getLogger(ApplicationExceptionHandler.class);

	@ResponseStatus(value = HttpStatus.NOT_FOUND)
	@ExceptionHandler({ NotFoundException.class })
	public @ResponseBody ResponseEntity<?> handleNotFoundException(Exception e) {
		logger.error("ERROR", e);
		return new ResponseEntity(new Error(e != null ? e.getClass().getSimpleName() : "NotFoundException", e != null ? e.getMessage() : "Not Found"), HttpStatus.NOT_FOUND);
	}

	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	@ExceptionHandler({ BadRequestException.class })
	public @ResponseBody ResponseEntity<?> handleBadRequestException(Exception e) {
		logger.error("ERROR", e);
		return new ResponseEntity(new Error(e != null ? e.getClass().getSimpleName() : "BadRequestException", e != null ? e.getMessage() : "Bad Request"), HttpStatus.BAD_REQUEST);
	}

	@ResponseStatus(value = HttpStatus.CONFLICT)
	@ExceptionHandler({ MongoWriteException.class })
	public @ResponseBody ResponseEntity<?> handleConflictException(Exception e) {
		logger.error("ERROR", e);
		return new ResponseEntity(new Error(e != null ? e.getClass().getSimpleName() : "ConflictException", e != null ? e.getMessage() : "Conflict"), HttpStatus.CONFLICT);
	}

}
