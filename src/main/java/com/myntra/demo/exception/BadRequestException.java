package com.myntra.demo.exception;

public class BadRequestException extends RuntimeException {

	private static final long serialVersionUID = 825243729971541969L;

	public BadRequestException(String message) {
		super(message);
	}
}
