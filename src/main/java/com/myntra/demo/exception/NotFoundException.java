package com.myntra.demo.exception;

public class NotFoundException extends RuntimeException {

	private static final long serialVersionUID = 825243729971541969L;

	public NotFoundException(String message) {
		super(message);
	}
}
