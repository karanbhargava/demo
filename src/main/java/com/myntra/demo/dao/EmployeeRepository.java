package com.myntra.demo.dao;

import com.myntra.demo.model.Employee;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface EmployeeRepository extends CrudRepository<Employee, Long> {

    Employee findById(long id);

    List<Employee> findByFirstName(String firstName);

    Employee findTopByFirstNameAndLastName(String firstName, String lastName);

}