package com.myntra.demo.dao;

import com.myntra.demo.model.Employee;
import com.myntra.demo.model.EmployeeMongo;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface EmployeeMongoRepository extends MongoRepository<EmployeeMongo, String> {

}