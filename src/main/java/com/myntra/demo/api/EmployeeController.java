package com.myntra.demo.api;


import com.myntra.demo.exception.BadRequestException;
import com.myntra.demo.model.Employee;
import com.myntra.demo.model.EmployeeMongo;
import com.myntra.demo.service.EmployeeService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/employee")
public class EmployeeController {

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Employee getEmployeeById(@PathVariable("id") long id) throws Exception {
        try {
            if( id <= 0) {
                throw new BadRequestException("Invalid input parameters.");
            }
            return employeeService.findById(id);
        } catch (IllegalArgumentException e) {
            throw new BadRequestException(e.getMessage());
        } catch (Exception e) {
            throw e;
        }
    }

    @RequestMapping(value = "/mongo/{id}", method = RequestMethod.GET)
    public EmployeeMongo getEmployeeMongoById(@PathVariable("id") String id) throws Exception {
        try {
            if(StringUtils.isBlank(id)) {
                throw new BadRequestException("Invalid input parameters.");
            }
            return employeeService.findByMongoId(id);
        } catch (IllegalArgumentException e) {
            throw new BadRequestException(e.getMessage());
        } catch (Exception e) {
            throw e;
        }
    }

    @RequestMapping(value = "/mongo", method = RequestMethod.POST)
    public EmployeeMongo saveEmployeeMongo(@RequestBody EmployeeMongo employeeMongo) throws Exception {
        try {
            if(employeeMongo == null) {
                throw new BadRequestException("Invalid input parameters.");
            }
            return employeeService.saveMongo(employeeMongo);
        } catch (IllegalArgumentException e) {
            throw new BadRequestException(e.getMessage());
        } catch (Exception e) {
            throw e;
        }
    }

    @Autowired
    private EmployeeService employeeService;

}