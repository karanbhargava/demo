## Spring Boot Demo Project
## Prerequisites
  
Install Mongo and Mysql on your local

---
Mysql

- Create database test
- import schema.sql present in src/main/resources folder in mysql

Mongo

- make sure localhost:27017 is active and mongodb is running on it
- create database test

---